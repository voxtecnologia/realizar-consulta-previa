# Desafio - Automatizar a realização de uma Consulta Previa

### Desafio VOX
Olá, queremos te desafiar a participar do nosso Time VOX. Podemos começar? Seu trabalho será visto pelo nosso time técnico e você receberá ao final um feedback sobre o seu deseafio. Interessante?

### Sobre a oportunidade 
A vaga é para Analista de Qualidade com foco em Automatização de Testes, temos vagas com desafios variados e para cada um deles utilizaremos critérios específicos considerando esse aspecto, combinado? 

Se você for aprovado nesta etapa, será convidado a apresentar suas soluções diante o time que te avaliou.

### Desafio Técnico
  Crie uma pequena aplicação que seja capaz de preencher uma consulta-previa no site http://homologacao.facilita.al.gov.br
  
  - Pré-requisitos:
    ```
    * Não utilizar ferramenta de gravação de cenário (Selenium IDE)
    * Poder executar o teste de forma VISUAL
    ```
    
  - Diferencias:
    ```
    * Utilização de Protractor (protractortest.org)
    * Preenchimento do formulário de forma dinâmica
    * Gerar relatório dos testes executados com sucesso e os que deram erro
    ```
    
  - O que deve ser feito:
    ```
    * Ao entrar no portal, a aplicação deve clicar no botão "Prosseguir";
    * Clicar no botão "Matriz"
    * Clicar no botão "Abertura"
    * Preencher o formulário de Consulta Prévia até o momento de gerar o Protocolo de acompanhamento
    ```
    
  - O que vamos avaliar:
    ```
    * Seu código; 
    * Boas práticas;
    * Organização;
    * Performance;
    ```

  - Dependências
    ```
    * https://homologacao.facilita.al.gov.br
    * Git
    ```

### Instruções
      1. Crie um repositório privado na gitlab.com;
      2. Desenvolva. Você terá 3 (três) dias a partir da data do envio do desafio; 
      3. Após concluir seu trabalho, publique; 
      4. Crie um arquivo README.MD com a explicação de como devemos executar o projeto e com uma descrição do que foi feito; 
      5. Adicione como master projeto o email philipe@voxtecnologia.com.br
      6. Pronto! Agora é so esperar o nosso feedback... Boa sorte!!
